import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FolderInfo } from '../../model/file-info';
import { HttpService } from '../../service/http.service';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css'],
})
export class TreeComponent implements OnInit {
  public data$: Observable<FolderInfo[]>;

  constructor(private httpService: HttpService) {}

  ngOnInit(): void {
    this.data$ = this.httpService.getFiles().pipe(
      map((data) => data.tree),
      tap((data) => console.log('data', data))
    );
  }
}
